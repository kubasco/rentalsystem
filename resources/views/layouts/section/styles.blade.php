<!-- Bootstrap CSS-->
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<!-- jQuery UI-->
<link href="{{ asset('css/jquery-ui.css') }}" rel="stylesheet">
<!-- Font Awesome CSS-->
<link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
<!-- Custom Font Icons CSS-->
<link href="{{ asset('css/font.css') }}" rel="stylesheet">
<!-- Google fonts - Muli-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,700">
<!-- theme stylesheet-->
<link href="{{ asset('css/styles.css') }}" rel="stylesheet">
<!-- Favicon-->
<link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
