<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    @include('layouts.section.styles')
    @include('layouts.section.scripts')

</head>
<body>

@guest
    @yield('loginForm')
@else

    @include('auth.section.header')

    <div class="d-flex align-items-stretch">
        <!-- Sidebar Navigation-->
    @include('auth.section.menu')
    <!-- Sidebar Navigation end-->
        <div class="page-content">

            @include('auth.section.panel_top')

            @yield('content')

            @include('auth.section.panel_footer')

        </div>
    </div>

@endguest



</body>
</html>
