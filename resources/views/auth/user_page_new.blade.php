@extends('layouts.app')

@section('content')

    <section class="no-padding-bottom">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-10">
                    <div class="user-block block text-center">

                        <form method="POST" action="{{ route('admin_save_new_user') }}">
                            @csrf

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.name') }}</label>
                                <div class="col-sm-9">
                                    <input name="name" type="text" class="form-control{!! $errors->first('name') ? ' is-invalid' : '' !!}"
                                           placeholder="{{ __('dashboard.user_add_input_name') }}" value="{{ old('name') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.email') }}</label>
                                <div class="col-sm-9">
                                    <input name="email" type="text" class="form-control{!! $errors->first('email') ? ' is-invalid' : '' !!}"
                                           placeholder="{{ __('dashboard.user_add_input_email') }}" value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.password') }}</label>
                                <div class="col-sm-9">
                                    <input name="password" type="text" class="form-control{!! $errors->first('password') ? ' is-invalid' : '' !!}"
                                           placeholder="{{ __('dashboard.user_add_input_password') }}" value="{{ old('password') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.phone') }}</label>
                                <div class="col-sm-9">
                                    <input name="phone" type="text" class="form-control{!! $errors->first('phone') ? ' is-invalid' : '' !!}"
                                           placeholder="{{ __('dashboard.user_add_input_phone') }}" value="{{ old('phone') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.address') }}</label>
                                <div class="col-sm-9">
                                    <textarea rows="6"
                                              name="address"
                                              class="form-control{!! $errors->first('address') ? ' is-invalid' : '' !!}"
                                              placeholder="{{ __('dashboard.user_add_input_address') }}">{{ old('address') }}</textarea>
                                </div>
                            </div>
                            <div class="line"></div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.zip_code') }}</label>
                                <div class="col-sm-9">
                                    <input name="zip_code" type="text" class="form-control{!! $errors->first('zip_code') ? ' is-invalid' : '' !!}"
                                           placeholder="{{ __('dashboard.user_add_input_zip_code') }}" value="{{ old('zip_code') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.city') }}</label>
                                <div class="col-sm-9">
                                    <input name="city" type="text" class="form-control{!! $errors->first('city') ? ' is-invalid' : '' !!}"
                                           placeholder="{{ __('dashboard.user_add_input_city') }}" value="{{ old('city') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.user_type') }}</label>
                                <div class="col-sm-9">
                                    <select name="type" class="form-control mb-3 mb-3" autocomplete="off">
                                        @foreach($user_types as $type)
                                            @if(old('type') === $type)
                                                <option value="{{ $type }}" selected>{{ __('dashboard.'.$type) }}</option>
                                            @else
                                                <option value="{{ $type }}">{{ __('dashboard.'.$type) }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="line"></div>

                            <button type="submit"
                                    class="btn btn-outline-success">{{ __('dashboard.save_changes') }}
                                / {{ __('dashboard.add_new_user') }}</button>

                        </form>

                    </div>
                </div>

                @include('auth.section.errors')

                <div class="col-lg-12">
                    <div class="block">
                        <a class="btn btn-outline-warning" href="{{ url()->previous() }}">
                            {{ __('dashboard.previous_view') }}
                        </a>
                        <a class="btn btn-outline-primary" href="{{ route('admin_users') }}">
                            {{ __('dashboard.admin_users') }}
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
