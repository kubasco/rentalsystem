@extends('layouts.app')

@section('content')

    <section class="no-padding-top">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-6">
                    <div class="block">
                        <div class="title">
                            <strong>{{ __('dashboard.orders') }}</strong>
                            <a class="btn btn-outline-success btn-sm"
                               href="{{ route('admin_add_new_order') }}">{{ __('dashboard.add_new_order') }}</a>
                        </div>
                        {{ $orders->links() }}
                        <hr>
                        <div class="table-responsive">
                            <table class="table table-striped table-sm">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>{{ __('dashboard.name') }}</th>
                                    <th>{{ __('dashboard.payed') }}</th>
                                    <th>{{ __('dashboard.status') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        <th scope="row">{{ $order->id }}</th>
                                        <td><a href="{{ route('admin_show_order', $order->id) }}">{{ $order->name }}</a></td>
                                        <td>{{ $order->paid }} {{ config('app.currency') }}</td>
                                        <td>{{ __('dashboard.'.$order->status) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <hr>
                            {{ $orders->links() }}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
