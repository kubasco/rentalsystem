@if (session('success'))
    <section class="no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 error_div" style="position: absolute; z-index:1000;">
                    <div class="user-block block text-center bg-success">
                        <h2 class="text-black">{{ session('success') }}</h2>
                    </div>
                </div>
                <script>
                    window.setTimeout(function () {
                        $(".error_div").fadeTo(500, 0).slideUp(500, function () {
                            $(this).remove();
                        });
                    }, 2000);
                </script>
            </div>
        </div>
    </section>
@endif
