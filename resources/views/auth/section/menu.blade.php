<nav id="sidebar">
    <!-- Sidebar Header-->
    <div class="sidebar-header d-flex align-items-center">
        <div class="avatar"><img src="{{ asset('img/avatar.png') }}" alt="..." class="img-fluid rounded-circle"></div>
        <div class="title">
            <h1 class="h5">{{ Auth::user()->name }}</h1>
            <p>{{ Auth::user()->type }}</p>
        </div>
    </div>
    <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
    <ul class="list-unstyled">
        <li @if(Route::current()->getName() === 'admin_panel') class="active" @endif><a
                href="{{ route('admin_panel') }}"> <i class="icon-home"></i>{{ __('dashboard.home') }} </a></li>
        <li @if(Route::current()->getName() === 'admin_orders' ||
            Route::current()->getName() === 'admin_show_order') class="active" @endif><a
                href="{{ route('admin_orders') }}"> <i class="icon-padnote"></i>{{ __('dashboard.orders') }} </a></li>
        <li @if(Route::current()->getName() === 'admin_users') class="active" @endif><a
                href="{{ route('admin_users') }}"> <i class="icon-grid"></i>{{ __('dashboard.users') }} </a></li>
        <li @if(Route::current()->getName() === 'admin_products' ||
            Route::current()->getName() === 'admin_show_product') class="active" @endif><a
                href="{{ route('admin_products') }}"> <i class="fa fa-bar-chart"></i>{{ __('dashboard.products') }} </a>
        </li>

        {{--         <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i--}}
        {{--                    class="icon-windows"></i>Example dropdown </a>--}}
        {{--            <ul id="exampledropdownDropdown" class="collapse list-unstyled ">--}}
        {{--                <li><a href="#">Page</a></li>--}}
        {{--                <li><a href="#">Page</a></li>--}}
        {{--                <li><a href="#">Page</a></li>--}}
        {{--            </ul>--}}
        {{--        </li>--}}
    </ul>
    {{--    <span class="heading">Extras</span>--}}
    {{--    <ul class="list-unstyled">--}}
    {{--        <li><a href="#"> <i class="icon-settings"></i>Demo </a></li>--}}
    {{--        <li><a href="#"> <i class="icon-writing-whiteboard"></i>Demo </a></li>--}}
    {{--        <li><a href="#"> <i class="icon-chart"></i>Demo </a></li>--}}
    {{--    </ul>--}}
</nav>
