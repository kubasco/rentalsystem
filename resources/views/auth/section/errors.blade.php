@if ($errors->any())
    <div class="col-lg-10 error_div" style="position: absolute;">
        <div class="user-block block text-center" style="border:5px solid red;">
            <h3>{{ __('dashboard.fix_errors') }}</h3>
            <hr>
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <button id="understand_button" class="btn btn-info">{{ __('dashboard.understand') }}</button>
        </div>
    </div>
    <script>
        window.setTimeout(function () {
            $(".error_div").fadeTo(500, 0).slideUp(500, function () {
                $(this).remove();
            });
        }, {{ $errors->count() * 3000 }});
        $("#understand_button").click(function()
        {
            $(".error_div").remove();
        });
    </script>
@endif
