<header class="header">
    <nav class="navbar navbar-expand-lg">
        <div class="container-fluid d-flex align-items-center justify-content-between">
            <div class="navbar-header">
                <!-- Navbar Header--><a href="{{ url('/') }}" class="navbar-brand">
                    <div class="brand-text brand-big visible text-uppercase"><strong
                            class="text-primary">Rental</strong><strong>System</strong></div>
                    <div class="brand-text brand-sm"><strong class="text-primary">R</strong><strong>S</strong></div>
                </a>
                <!-- Sidebar Toggle Btn-->
                <button class="sidebar-toggle"><i class="fa fa-long-arrow-left"></i></button>
            </div>
            <div class="right-menu list-inline no-margin-bottom">
                <!-- Languages dropdown    -->
                <div class="list-inline-item dropdown">
                    <a id="languages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" class="nav-link language dropdown-toggle">
                        <img src="{{ asset('img/flags/'.LaravelLocalization::getCurrentLocale()) }}.png"
                             alt="{{ LaravelLocalization::getCurrentLocaleName() }}">
                        <span class="d-none d-sm-inline-block">{{ LaravelLocalization::getCurrentLocaleName() }}</span>
                    </a>
                    <div aria-labelledby="languages" class="dropdown-menu">
                        @foreach(config('laravellocalization.supportedLocales') as $localeCode => $localization)
                            <a rel="nofollow" href="{{ LaravelLocalization::getLocalizedURL($localeCode) }}" class="dropdown-item">
                                <img src="{{ asset('img/flags/'.$localeCode) }}.png" alt="{{ $localization['name'] }}"
                                     class="mr-2">
                                <span>{{ ucfirst($localization['native']) }}</span>
                            </a>
                        @endforeach
                    </div>
                </div>
                <!-- Log out               -->
                <div class="list-inline-item logout">
                    <a id="logout" class="nav-link" href="{{ route('admin_logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"> <span
                            class="d-none d-sm-inline">{{ __('dashboard.logout') }}</span>
                        <i class="icon-logout"></i>
                    </a>
                    <form id="logout-form" action="{{ route('admin_logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </nav>
</header>
