@extends('layouts.app')

@section('content')

    <section class="no-padding-bottom">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-10">
                    <div class="user-block block text-center">

                        <form method="POST" action="{{ route('admin_save_new_product') }}">
                            @csrf

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.product_name') }}</label>
                                <div class="col-sm-9">
                                    <input name="name" type="text" class="form-control{!! $errors->first('name') ? ' is-invalid' : '' !!}"
                                           placeholder="{{ __('dashboard.product_add_input_name') }}" value="{{ old('name') }}">
                                </div>
                            </div>
                            <div class="line"></div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.description') }}</label>
                                <div class="col-sm-9">
                                    <textarea rows="6"
                                              name="description"
                                              class="form-control{!! $errors->first('description') ? ' is-invalid' : '' !!}"
                                              placeholder="{{ __('dashboard.product_add_input_name') }}">{{ old('description') }}</textarea>
                                </div>
                            </div>
                            <div class="line"></div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.price') }}</label>
                                <div class="col-sm-7">
                                    <input name="price" type="text" class="form-control{!! $errors->first('price') ? ' is-invalid' : '' !!}"
                                           placeholder="{{ __('dashboard.product_add_input_name') }}" value="{{ old('price') }}">
                                </div>
                            </div>

                            <div class="line"></div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.status') }}</label>
                                <div class="col-sm-9">
                                    <select name="status" class="form-control mb-3 mb-3" autocomplete="off">
                                        @foreach($statuses as $status)
                                            <option value="{{ $status }}">{{ $status }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="line"></div>

                            <button type="submit"
                                    class="btn btn-outline-success">{{ __('dashboard.save_changes') }}
                                / {{ __('dashboard.add_new_product') }}</button>

                        </form>

                    </div>
                </div>

                @include('auth.section.errors')

                <div class="col-lg-12">
                    <div class="block">
                        <a class="btn btn-outline-warning" href="{{ url()->previous() }}">
                            {{ __('dashboard.previous_view') }}
                        </a>
                        <a class="btn btn-outline-primary" href="{{ route('admin_products') }}">
                            {{ __('dashboard.admin_products') }}
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
