@extends('layouts.app')

@section('content')

    @include('auth.section.success')

    <section class="no-padding-bottom">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-8">
                    <div class="user-block block text-center">
                        <h5>{{ __('dashboard.who_order') }}</h5>

                        <a class="btn btn-outline-success btn-sm"
                           href="{{ route('admin_edit_order', $order->id) }}">{{ __('dashboard.edit') }}</a>

                        <hr>
                        <h3>{{ $order->name }}</h3>
                        <p>{{ $order->phone }}</p>
                        <p>{{ $order->email }}</p>
                        @if($order->document_type)
                            <p><strong>{{ __('dashboard.' . $order->document_type) }}</strong>:
                                <br>{{ $order->document_number }}</p>
                        @endif
                        <p><strong>{{ __('dashboard.client_address') }}</strong>:
                            <br> {{ $order->address }}</p>
                        @if($order->comment)
                            <p><b>{{ __('dashboard.notes') }} : </b>
                                <br>{{ $order->comment }}</p>
                        @endif

                        @if($order->other_location === null)
                            <div class="contributions">{{ __('dashboard.pickup_at_partner') }}</div>
                        @else
                            <div class="contributions"><b>{{ __('dashboard.pickup_at') }}
                                    : </b>{{ $order->other_location }}</div>
                        @endif

                        @if($order->return_location === null)
                            <div class="contributions">{{ __('dashboard.return_location_at_partner') }}</div>
                        @else
                            <div class="contributions"><b>{{ __('dashboard.return_location') }}
                                    : </b>{{ $order->return_location }}</div>
                        @endif

                        @if($order->paid)
                            <div class="contributions"><b>{{ __('dashboard.payed') }}
                                    : </b>{{ $order->paid }} {{ config('app.currency') }}</div>
                        @endif

                        <p class="contributions"><i class="icon-info"></i><br>
                            <strong>{{ __('dashboard.from') }}
                                :</strong> {{ date('H:i / d.m.Y', strtotime($order->book_from)) }}
                            <br>
                            <strong>{{ __('dashboard.to') }}
                                :</strong> {{ date('H:i / d.m.Y', strtotime($order->book_to)) }}
                        </p>
                    </div>
                </div>

                @if($client)
                    <div class="col-lg-4">
                        <div class="user-block block text-center">
                            <h5>{{ __('dashboard.partner_data') }}</h5>
                            <hr>
                            <h3>{{ $client->name }}</h3>
                            <p>{{ $client->phone }}</p>
                            <p>{{ $client->email }}</p>

                            <p><b>{{ __('dashboard.partner_address') }} : </b>
                                <br>{{ $client->address }}
                                <br>{{ $client->zip_code }}, {{ $client->city }}
                            </p>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>

    <section class="no-padding-top">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-8">
                    <div class="block">
                        <div class="title"><strong>{{ __('dashboard.ordered_items') }}</strong>
                            <a class="btn btn-outline-success btn-sm"
                               href="{{ route('admin_edit_order_products', $order->id) }}">{{ __('dashboard.edit') }}</a>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>{{ __('dashboard.product') }}</th>
                                    <th>{{ __('dashboard.quantity') }} x {{ __('dashboard.price') }}</th>
                                    <th>{{ __('dashboard.total_price') }}</th>
                                </tr>
                                </thead>
                                <tbody>

                                @php $sum = null; @endphp
                                @foreach($order_products as $product)
                                    <tr>
                                        <th scope="row">{{ $product->id }}</th>
                                        <td>
                                            <a href="{{ route('admin_show_product', $product->id) }}">
                                                {{ $products_data_by_id[$product->products_id]['name'] }}
                                            </a>
                                        </td>
                                        <td>{{ $product->price }} {{ config('app.currency') }}
                                            x {{ $product->quantity }}</td>
                                        <td>{{ number_format($product->price * $product->quantity, 2, ',', ' ') }} {{ config('app.currency') }}
                                        </td>
                                    </tr>
                                    @php $sum += $product->price * $product->quantity @endphp
                                @endforeach
                                @if(!$order_products->first())
                                    <tr>
                                        <td colspan="4">{{ __('dashboard.no_results') }}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="2"></td>
                                        <td><b>{{ __('dashboard.sum') }} :</b></td>
                                        <td><b>{{ number_format($sum, 2, ',', ' ') }} {{ config('app.currency') }}</b></td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="block">
                        <a class="btn btn-outline-warning" href="{{ url()->previous() }}">
                            {{ __('dashboard.previous_view') }}
                        </a>
                        <a class="btn btn-outline-primary" href="{{ route('admin_orders') }}">
                            {{ __('dashboard.admin_orders') }}
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
