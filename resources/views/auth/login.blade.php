@extends('layouts.app')

@section('loginForm')

    <div class="login-page">
        <div class="container d-flex align-items-center">
            <div class="form-holder has-shadow">
                <div class="row">
                    <!-- Logo & Information Panel-->
                    <div class="col-lg-6">
                        <div class="info d-flex align-items-center">
                            <div class="content">
                                <div class="logo">
                                    <h1>{{ __('dashboard.dashboard') }}</h1>
                                </div>
                                <p>{{ config('app.name') }} v.{{ config('app.version') }}</p>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        Errors :
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- Form Panel    -->
                    <div class="col-lg-6">
                        <div class="form d-flex align-items-center">
                            <div class="content">

                                <form method="POST" class="form-validate mb-4" action="{{ route('admin_login') }}">
                                    @csrf
                                    <div class="form-group">
                                        <input id="email" type="text" name="email"
                                               class="input-material @error('email') is-invalid @enderror"
                                               value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        <label for="login-username"
                                               class="label-material">{{ __('dashboard.input_email') }}</label>
                                    </div>
                                    <div class="form-group">
                                        <input id="password" type="password" name="password"
                                               class="input-material @error('password') is-invalid @enderror"
                                               required autocomplete="current-password">
                                        <label for="login-password"
                                               class="label-material">{{ __('dashboard.input_password') }}</label>
                                    </div>

                                    <button type="submit" class="btn btn-primary">{{ __('dashboard.login') }}</button>
                                </form>

                                @foreach(config('laravellocalization.supportedLocales') as $localeCode => $localization)
                                    <a rel="nofollow"
                                       href="{{ LaravelLocalization::getLocalizedURL($localeCode) }}"><img
                                            src="{{ asset('img/flags/'.$localeCode) }}.png"
                                            style="width:25px !important;"
                                            alt="{{ $localization['name'] }}" class="mr-2"></a>
                                @endforeach

                                @if (Route::has('password.request'))
                                    <a class="forgot-pass" href="{{ route('password.request') }}">
                                        {{ __('dashboard.forgot_password') }}
                                    </a>
                                @endif
                                <br>
                                {{--                                <small>{{ __('dashboard.do_not_have_account') }} </small>--}}
                                {{--                                <a href="{{ route('register') }}" class="signup">{{ __('dashboard.button_register') }}</a>--}}
                                {{--                                <br>--}}
                                {{--                                <input class="form-check-input" type="checkbox" name="remember"--}}
                                {{--                                       id="remember" {{ old('remember') ? 'checked' : '' }}>--}}
                                {{--                                <label class="form-check-label" for="remember">--}}
                                {{--                                    {{ __('dashboard.remember_me') }}--}}
                                {{--                                </label>--}}
                            </div>
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </div>

@endsection
