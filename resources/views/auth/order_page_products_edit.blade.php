@extends('layouts.app')

@section('content')

    <section class="no-padding-bottom">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-8">
                    <div class="user-block block text-center">

                        <form method="POST" action="{{ route('admin_update_order_products', $order->id) }}">
                            @csrf
                            @method('PUT')

                            <div class="table-responsive">
                                <table id="productsTable" class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>{{ __('dashboard.product') }}</th>
                                        <th>{{ __('dashboard.price_one') }}</th>
                                        <th>{{ __('dashboard.quantity') }}</th>
                                        <th>{{ __('dashboard.total_price') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody id="productsTableBody">
                                    @foreach($orders_products as $product)
                                        <tr>
                                            <th scope="row">{{ $product->id }}</th>
                                            <td>{{ $products_data_by_id[$product->id]['name'] }}</td>
                                            <td>{{ $product->price }} {{ config('app.currency') }}</td>
                                            <td>{{ $product->quantity }}</td>
                                            <td>{{ number_format($product->price * $product->quantity, 2, ',', ' ') }} {{ config('app.currency') }}
                                            </td>
                                            <td><i id="{{ $product->id }}" class="btn btn-outline-danger btn-sm" onclick="deleteItem(this);"><i class="fa fa-close"></i></i></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <input id="productsTableData" name="products" type="hidden" value="{{ json_encode($orders_products->toArray()) }}">
                            </div>
                            <br>
                            <hr>
                            <div class="form-group row">
                                <label class="col-sm-2 form-control-label">{{ __('dashboard.choose_product') }}</label>
                                <select id="listItems" class="form-control col-sm-6" autocomplete="off">
                                    @foreach($products as $product)
                                        <option value="{{ json_encode($product) }}">{{ $product->name }} / {{ $product->price }} / {{ config('app.currency') }}</option>
                                    @endforeach
                                </select>
                                <i id="addItemToTable" class="offset-1 col-sm-2 btn btn-outline-info">{{ __('dashboard.add_product_to_list') }}</i>
                            </div>
                            <br>
                            <hr>
                            <button type="submit" class="btn btn-outline-success">{{ __('dashboard.save_changes') }}</button>

                        </form>

                    </div>
                </div>

                <script>
                    $("#listItems").on('keyup', function (e) {
                        if (e.keyCode === 13) {
                            addProductToList();
                        }
                    });

                    $("#addItemToTable").click(function() {
                        addProductToList();
                    });

                    function addProductToList(){
                        let productsTableData = JSON.parse($("#productsTableData").val());
                        let product = JSON.parse($("#listItems").val());
                        let productFound = false;

                        let arrayLength = productsTableData.length;
                        for (let index = 0; index < arrayLength; index++) {
                            if(productsTableData[index].id === product.id){
                                productFound = index;
                            }
                        }

                        if(productFound !== false) {
                            productsTableData[productFound].quantity = productsTableData[productFound].quantity+1;
                            productsTableData[productFound].price = product.price;
                            productsTableData[productFound].name = product.name;
                            productsTableData[productFound].currency = "{{ config('app.currency') }}";
                        }else{
                            productsTableData.push({
                                currency: "{{ config('app.currency') }}",
                                id: product.id,
                                name: product.name,
                                price: product.price,
                                quantity: 1
                            });
                        }

                        $("#productsTableBody").html("");

                        productsTableData.forEach(function(item) {
                            $("#productsTable").append(
                                "<tr>" +
                                "<th scope='row'>"+item.id+"</th>" +
                                "<td>"+item.name+"</td>" +
                                "<td>"+item.price+" "+"{{ config('app.currency') }}"+"</td>" +
                                "<td>"+item.quantity+"</td>" +
                                "<td>"+(item.price*item.quantity).toFixed(2)+" "+"{{ config('app.currency') }}"+"</td>" +
                                "<td><i id='"+item.id+"' class='btn btn-outline-danger btn-sm' onclick='deleteItem(this);'><i class='fa fa-close'></i></i></td>" +
                                "</tr>"
                            );
                        });

                        $("#productsTableData").val(JSON.stringify(productsTableData));
                    }

                    function deleteItem(e) {
                        let productsTableData = JSON.parse($("#productsTableData").val());
                        let productToDelete = e.id;

                        let arrayLength = productsTableData.length;
                        for (let index = 0; index < arrayLength; index++) {
                            if (parseInt(productsTableData[index].id) === parseInt(productToDelete)) {
                                productsTableData.splice(index, 1);
                                break;
                            }
                        }

                        $("#productsTableBody").html("");

                        productsTableData.forEach(function (item) {
                            $("#productsTable").append(
                                "<tr>" +
                                "<th scope='row'>" + item.id + "</th>" +
                                "<td>" + item.name + "</td>" +
                                "<td>" + item.price + " " + "{{ config('app.currency') }}" + "</td>" +
                                "<td>" + item.quantity + "</td>" +
                                "<td>" + (item.price * item.quantity).toFixed(2) + " " + "{{ config('app.currency') }}" + "</td>" +
                                "<td><i id='" + item.id + "' class='btn btn-outline-danger btn-sm' onclick='deleteItem(this);'><i class='fa fa-close'></i></i></td>" +
                                "</tr>"
                            );
                        });

                        $("#productsTableData").val(JSON.stringify(productsTableData));
                    }
                </script>

                @if ($errors->any())
                    <div class="col-lg-4 error_div">
                        <div class="user-block block text-center">
                            <h3>{{ __('dashboard.fix_errors') }}</h3>
                            <hr>
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <script>
                        window.setTimeout(function () {
                            $(".error_div").fadeTo(500, 0).slideUp(500, function () {
                                $(this).remove();
                            });
                        }, {{ $errors->count() * 3000 }});
                    </script>
                @endif

                <div class="col-lg-12">
                    <div class="block">
                        <a class="btn btn-outline-warning" href="{{ url()->previous() }}">
                            {{ __('dashboard.previous_view') }}
                        </a>
                        <a class="btn btn-outline-primary" href="{{ route('admin_orders') }}">
                            {{ __('dashboard.admin_orders') }}
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
