@extends('layouts.app')

@section('content')

    @include('auth.section.success')

    <section class="no-padding-top">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12">
                    <div class="block">
                        <a class="btn btn-outline-success btn-sm"
                           href="{{ route('admin_add_new_user') }}">{{ __('dashboard.add_new_user') }}</a>
                    </div>
                </div>

                @foreach($users_types as $type)

                    <div class="col-lg-12">
                        <div class="block">
                            <div class="title"><strong>{{ __('dashboard.'.$type) }}</strong></div>
                            <div class="table-responsive">
                                <table class="table table-striped table-sm">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>{{ __('dashboard.name') }}</th>
                                        <th>{{ __('dashboard.email') }}</th>
                                        <th>{{ __('dashboard.phone') }}</th>
                                        <th>{{ __('dashboard.address') }}</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users[$type] as $user)
                                        @if($user->type === $type)
                                            <tr>
                                                <th scope="row">{{ $user->id }}</th>
                                                <td>{{ $user->name }}</td>
                                                <td>{{ $user->email }}</td>
                                                <td>{{ $user->phone }}</td>
                                                <td>{{ $user->address }}<br>{{ $user->zip_code }}, {{ $user->city }}</td>
                                                <td><a href="{{ route('admin_edit_user', $user->id) }}"><i class="fa fa-edit text-warning"></i></a></td>
                                                <td><a href="{{ route('admin_delete_user', $user->id) }}" onclick="return confirm('{{ __('dashboard.confirm_delete') }}')"><i class="fa fa-close text-danger"></i></a></td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                @endforeach

            </div>
        </div>
    </section>

@endsection
