@extends('layouts.app')

@section('content')

    @include('auth.section.success')

    <section class="no-padding-bottom">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-8">
                    <div class="user-block block text-center">
                        <h5>{{ $product->name }}</h5>
                        <a class="btn btn-outline-success btn-sm" href="{{ route('admin_edit_product', $product->id) }}">{{ __('dashboard.edit') }}</a>
                        <hr>

                        <p>{{ $product->description }}</p>

                        <div class="contributions">{{ $product->price }} {{ config('app.currency') }}</div>

                        @if($product->status === 'active')
                            <div class="contributions">{{ __('dashboard.active') }}</div>
                        @else
                            <div class="contributions">{{ __('dashboard.disabled') }}</div>
                        @endif
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="block">
                        <a class="btn btn-outline-warning" href="{{ url()->previous() }}">
                            {{ __('dashboard.previous_view') }}
                        </a>
                        <a class="btn btn-outline-primary" href="{{ route('admin_products') }}">
                            {{ __('dashboard.admin_products') }}
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
