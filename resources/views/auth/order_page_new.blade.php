@extends('layouts.app')

@section('content')

    <section class="no-padding-bottom">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-10">
                    <div class="user-block block text-center">

                        <form method="POST" action="{{ route('admin_save_new_order') }}">
                            @csrf

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.who_order') }}</label>
                                <div class="col-sm-9">
                                    <input name="name" type="text"
                                           class="form-control{!! $errors->first('name') ? ' is-invalid' : '' !!}"
                                           value="{{ old('name') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.phone') }}</label>
                                <div class="col-sm-7">
                                    <input name="phone" type="text"
                                           class="form-control{!! $errors->first('phone') ? ' is-invalid' : '' !!}"
                                           value="{{ old('phone') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.documentType') }}</label>
                                <div class="col-sm-9">
                                    <select name="document_type" class="form-control mb-3 mb-3" autocomplete="off">
                                        @foreach($documentTypes as $documentType)
                                            <option value="{{ $documentType }}"
                                                @if(old('document_type'))
                                                    @if($documentType === old('document_type')) selected @endif
                                                @endif
                                            >{{ __('dashboard.'.$documentType) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.document_number') }}</label>
                                <div class="col-sm-7">
                                    <input name="document_number" type="text"
                                           class="form-control{!! $errors->first('document_number') ? ' is-invalid' : '' !!}"
                                           value="{{ old('document_number') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.email') }}</label>
                                <div class="col-sm-7">
                                    <input name="email" type="text"
                                           class="form-control{!! $errors->first('email') ? ' is-invalid' : '' !!}"
                                           value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.client_address') }}</label>
                                <div class="col-sm-9">
                                    <input name="address" type="text"
                                           class="form-control{!! $errors->first('address') ? ' is-invalid' : '' !!}"
                                           value="{{ old('address') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.notes') }}</label>
                                <div class="col-sm-9">
                                    <textarea rows="4" name="comment"
                                              class="form-control{!! $errors->first('comment') ? ' is-invalid' : '' !!}">{{ old('comment') }}</textarea>
                                </div>
                            </div>

                            <hr>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.book_from') }}</label>
                                <div class="col-sm-3">{{ __('dashboard.date') }}
                                    <input name="book_from_date" type="text"
                                           class="form-control{!! $errors->first('book_from_date') ? ' is-invalid' : '' !!}"
                                           value="{{ old('book_from_date') }}" readonly>
                                </div>
                                <div class="col-sm-2">{{ __('dashboard.hour') }}
                                    <select name="book_from_hour" class="form-control mb-3 mb-3" autocomplete="off">
                                        @foreach(range(1,23,1) as $hour)
                                            <option value="{{ $hour }}"
                                                    @if(old('book_from_hour'))
                                                        @if($hour == old('book_from_hour')) selected @endif
                                                    @endif
                                                    >{{ $hour }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-2">{{ __('dashboard.minute') }}
                                    <select name="book_from_minute" class="form-control mb-3 mb-3" autocomplete="off">
                                        @foreach(range(1,59,1) as $minute)
                                            <option value="{{ $minute }}"
                                                    @if(old('book_from_minute'))
                                                        @if($minute == old('book_from_minute')) selected @endif
                                                    @endif
                                                    >{{ $minute }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.book_to') }}</label>
                                <div class="col-sm-3">{{ __('dashboard.date') }}
                                    <input name="book_to_date" type="text"
                                           class="form-control{!! $errors->first('book_to_date') ? ' is-invalid' : '' !!}"
                                           value="{{ old('book_to_date') }}" readonly>
                                </div>
                                <div class="col-sm-2">{{ __('dashboard.hour') }}
                                    <select name="book_to_hour" class="form-control mb-3 mb-3" autocomplete="off">
                                        @foreach(range(1,23,1) as $hour)
                                            <option value="{{ $hour }}"
                                                    @if(old('book_to_hour'))
                                                        @if($hour == old('book_to_hour')) selected @endif
                                                    @endif
                                            >{{ $hour }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-2">{{ __('dashboard.minute') }}
                                    <select name="book_to_minute" class="form-control mb-3 mb-3" autocomplete="off">
                                        @foreach(range(1,59,1) as $minute)
                                            <option value="{{ $minute }}"
                                                    @if(old('book_to_minute'))
                                                        @if($minute == old('book_to_minute')) selected @endif
                                                    @endif
                                            >{{ $minute }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <script>
                                $( function() {
                                    $( "input[name='book_from_date']" ).datepicker({ dateFormat: 'yy-mm-dd' });
                                    $( "input[name='book_to_date']" ).datepicker({ dateFormat: 'yy-mm-dd' });
                                } );
                            </script>

                            <hr>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.payed') }}</label>
                                <div class="col-sm-7">
                                    <input name="paid" type="text"
                                           class="form-control{!! $errors->first('paid') ? ' is-invalid' : '' !!}"
                                           value="{{ old('paid') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.pickup_at_partner') }}</label>
                                <div class="col-sm-9">
                                    <select name="other_location" class="form-control mb-3 mb-3" autocomplete="off">
                                        <option value="yes"
                                            @if(old('other_location'))
                                                @if('yes' === old('other_location')) selected @endif
                                            @endif
                                        >{{ __('dashboard.yes') }}</option>
                                        <option value="no"
                                            @if(old('other_location'))
                                                @if('no' === old('other_location')) selected @endif
                                            @endif
                                        >{{ __('dashboard.no') }}</option>
                                    </select>
                                </div>
                            </div>

                            <div id="pickup_data" class="form-group row"
                                 @if(old('other_location') )
                                    @if(old('other_location') === 'yes' )
                                        style="display: none;"
                                    @endif
                                 @endif
                            >
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.pickup_at') }}</label>
                                <div class="col-sm-9">
                                    <textarea rows="4" name="other_location_data"
                                              class="form-control{!! $errors->first('other_location_data') ? ' is-invalid' : '' !!}">{{ old('other_location_data') }}</textarea>
                                </div>
                            </div>

                            <script>
                                $('select[name="other_location"]').on('change', function() {
                                   if ($(this).val() === 'no'){
                                       $('#pickup_data').show().addClass('bounceIn');
                                   }else{
                                       $('#pickup_data').hide().removeClass('bounceIn');
                                   }
                                });
                            </script>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.return_location_at_partner') }}</label>
                                <div class="col-sm-9">
                                    <select name="return_location" class="form-control mb-3 mb-3" autocomplete="off">
                                        <option value="yes"
                                            @if(old('return_location'))
                                                @if('yes' === old('return_location')) selected @endif
                                            @endif
                                        >{{ __('dashboard.yes') }}</option>
                                        <option value="no"
                                            @if(old('return_location'))
                                                @if('no' === old('return_location')) selected @endif
                                            @endif
                                        >{{ __('dashboard.no') }}</option>
                                    </select>
                                </div>
                            </div>

                            <div id="return_data" class="form-group row"
                                @if(old('return_location') )
                                     @if(old('return_location') === 'yes' )
                                     style="display: none;"
                                     @endif
                                @endif
                            >
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.return_location') }}</label>
                                <div class="col-sm-9">
                                    <textarea rows="4" name="return_location_data"
                                              class="form-control{!! $errors->first('return_location_data') ? ' is-invalid' : '' !!}">{{ old('return_location_data') }}</textarea>
                                </div>
                            </div>

                            <script>
                                $('select[name="return_location"]').on('change', function() {
                                    if ($(this).val() === 'no'){
                                        $('#return_data').show().addClass('bounceIn');
                                    }else{
                                        $('#return_data').hide().removeClass('bounceIn');
                                    }
                                });
                            </script>

                            <hr>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.status') }}</label>
                                <div class="col-sm-9">
                                    <select name="status" class="form-control mb-3 mb-3" autocomplete="off">
                                        @foreach($statuses as $status)
                                            <option value="{{ $status }}"
                                                @if(old('status'))
                                                    @if($status === old('status')) selected @endif
                                                @endif
                                            >{{ __('dashboard.'.$status) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <button type="submit"
                                    class="btn btn-outline-success">{{ __('dashboard.save') }}</button>

                        </form>

                    </div>
                </div>

                @include('auth.section.errors')

                <div class="col-lg-12">
                    <div class="block">
                        <a class="btn btn-outline-warning" href="{{ url()->previous() }}">
                            {{ __('dashboard.previous_view') }}
                        </a>
                        <a class="btn btn-outline-primary" href="{{ route('admin_orders') }}">
                            {{ __('dashboard.admin_orders') }}
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
