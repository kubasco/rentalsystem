@extends('layouts.app')

@section('content')

    <section class="no-padding-top">
        <div class="container-fluid">
            <div class="row">

                    <div class="col-lg-10">
                        <div class="block">
                            <div class="title">
                                <strong>{{ __('dashboard.products') }}</strong>
                                <a class="btn btn-outline-warning btn-sm" href="{{ route('admin_new_product') }}">
                                    {{ __('dashboard.add_new_product') }}
                                </a>
                            </div>

                            {{ $products->links() }}
                            <hr>
                            <div class="table-responsive">
                                <table class="table table-striped table-sm">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>{{ __('dashboard.name') }}</th>
                                        <th>{{ __('dashboard.description') }}</th>
                                        <th>{{ __('dashboard.price') }}</th>
                                        <th>{{ __('dashboard.currency') }}</th>
                                        <th>{{ __('dashboard.status') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $product)
                                        @if($product->status === 'active')
                                            @php $colorTd = '#2d5221'; @endphp
                                        @else
                                            @php $colorTd = '#471d1d'; @endphp
                                        @endif
                                            <tr>
                                            <th scope="row" style="background-color: {{ $colorTd }} !important;">{{ $product->id }}</th>
                                            <td style="background-color: {{ $colorTd }} !important;">
                                                <a href="{{ route('admin_show_product', $product->id) }}">
                                                    {{ $product->name }}
                                                </a>
                                            </td>
                                            <td style="background-color: {{ $colorTd }} !important;">{{ $product->description }}</td>
                                            <td style="background-color: {{ $colorTd }} !important;">{{ $product->price }}</td>
                                            <td style="background-color: {{ $colorTd }} !important;">{{ config('app.currency') }}</td>
                                            <td style="background-color: {{ $colorTd }} !important;">{{ __('dashboard.'.$product->status) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <hr>
                                {{ $products->links() }}
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </section>

@endsection
