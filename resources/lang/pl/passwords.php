<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Twoje hasło zostało zresetowane!',
    'sent' => 'Wysłano link do resetu hasła!',
    'token' => 'Token resetowania hasła jest zły',
    'user' => "Nie ma takiego użytkownika",
    'throttled' => 'Odczekaj chwilę i ponów operację',

];
