## About RentalSystem project

System for rent products in b2b system.

How it works ?

Company A : wants to rent something for clients in Company B, so there clients can simple (on tablet, or PC) choose product to rent from Company A.

Example :

Client is in restaurant and want to rent a scooter after drink a coffe. Restaurant has a familiar company which rents scooters. So the have computer where client can order scooter for a specifi period of time.

- no payments
- only reservation system for now

## Installation

1. open console in project folder
2. copy .env.example to .env
3. set Your environment if different in ".env" and ".env.testing"
4. composer install
5. create tables like given in .env files : "rentalsystem" and "rentalsystem_testing"
6. php artisan migrate:fresh --seed

## Testing

1. run tests with PHPUnit :
- vendor\bin\phpunit --debug
- it will use table "rentalsystem_testing"

## Helpers

1. IDE HELPER for models
php artisan ide-helper:models Users

##Additional Information
1. test admin account - login: admin@example.com, password: test (defined in development seeder)
