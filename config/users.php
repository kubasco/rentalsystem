<?php

return [
    'types' => [
        'admin' => 'admin',
        'worker' => 'worker',
        'partner' => 'partner',
    ]
];
