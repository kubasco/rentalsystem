<?php

use App\Orders;
use App\OrdersProducts;
use App\Products;
use App\Users;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;

class DevelopmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * USERS
         */
        factory(Users::class)->create([
            'name' => 'Admin Test',
            'email' => 'admin@example.com',
            'password' => Hash::make('password'),
            'type' => 'admin'
        ]);

        $usersTypes = Config::get('users.types');

        foreach ($usersTypes as $type) {
            factory(Users::class, 5)->create([
                'type' => $type
            ]);
        }

        /**
         * PRODUCTS
         */
        $statuses = ['active', 'disabled'];

        foreach ($statuses as $status) {
            factory(Products::class, 15)->create([
                'status' => $status
            ]);
        }

        /**
         * ORDERS
         */
        for ($id = 2; $id <= 6; $id++) {
            factory(Orders::class)->create([
                'users_id' => $id
            ]);
        }

        /**
         * ORDERS_PRODUCTS
         */
        for ($id = 1; $id <= 5; $id++) {
            factory(OrdersProducts::class)->create([
                'orders_id' => $id,
                'products_id' => $id,
            ]);
            factory(OrdersProducts::class)->create([
                'orders_id' => $id,
                'products_id' => $id + 2,
            ]);
            factory(OrdersProducts::class)->create([
                'orders_id' => $id,
                'products_id' => $id + 4,
            ]);
        }

    }
}
