<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('users_id')->unsigned()->default(null);
            $table->timestamp('book_from')->nullable()->default(null);
            $table->timestamp('book_to')->nullable()->default(null);
            $table->timestamp('start_date')->nullable()->default(null);
            $table->timestamp('end_date')->nullable()->default(null);
            $table->float('paid', 8, 2)->nullable()->default(null);
            $table->string('name',128);
            $table->string('address',512)->nullable()->default(null);
            $table->string('document_number',128)->nullable()->default(null);
            $table->enum('document_type',['identity_card','driving_license'])->nullable()->default(null);
            $table->string('phone',32);
            $table->string('email',128)->nullable()->default(null);
            $table->text('comment')->nullable()->default(null);
            $table->text('other_location')->nullable()->default(null);
            $table->text('return_location')->nullable()->default(null);
            $table->enum('status',['active','disabled'])->default('active');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('users_id')->references('id')->on('users')->delete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
