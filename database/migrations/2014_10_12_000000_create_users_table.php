<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 128);
            $table->string('email', 128);
            $table->string('password', 256);
            $table->string('phone', 32)->nullable();
            $table->string('address', 128)->nullable();
            $table->string('zip_code', 16)->nullable();
            $table->string('city', 128)->nullable();
            $table->enum('type',['admin','worker','partner'])->nullable();

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            $table->unique('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
