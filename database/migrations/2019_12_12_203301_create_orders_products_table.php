<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('orders_id')->unsigned();
            $table->bigInteger('products_id')->unsigned();
            $table->float('price', 8, 2);
            $table->integer('quantity');

            $table->timestamps();

            $table->unique(['orders_id', 'products_id']);

            $table->foreign('orders_id')->references('id')->on('orders')->delete('cascade');
            $table->foreign('products_id')->references('id')->on('products')->delete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_products');
    }
}
