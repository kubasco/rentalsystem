<?php

/** @var Factory $factory */

use App\Orders;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Orders::class, function (Faker $faker) {

    $bookFrom = $faker->dateTimeBetween($startDate = '-12 hours', $endDate = 'now');
    $bookTo = $faker->dateTimeBetween($startDate = 'now', $endDate = '+12 hours');

    return [
//        'users_id' => $id,
        'book_from' => $bookFrom,
        'book_to' => $bookTo,
        'start_date' => $bookFrom,
        'end_date' => $bookTo,
        'paid' => mt_rand(1, 500),
        'name' => $faker->name,
        'address' => $faker->address,
        'document_number' => $faker->numberBetween(1234567,1234567890),
        'document_type' => ['identity_card','driving_license'][mt_rand(0, 1)],
        'phone' => $faker->phoneNumber,
        'email' => $faker->email,
        'comment' => $faker->text,
        'other_location' => [null, $faker->address][mt_rand(0, 1)],
        'return_location' => [null, $faker->address][mt_rand(0, 1)],
        'status' => ['active', 'disabled'][mt_rand(0, 1)]
    ];
});
