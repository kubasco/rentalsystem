<?php

/** @var Factory $factory */

use App\Users;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

$factory->define(Users::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => Hash::make('password'), // password
        'phone' => $faker->phoneNumber,
        'address' => $faker->streetAddress,
        'zip_code' => $faker->postcode,
        'city' => $faker->city,
        'type' => ['admin','worker','partner'][mt_rand(0,1)],
        'remember_token' => Str::random(10)
    ];
});
