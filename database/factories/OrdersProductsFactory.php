<?php

/** @var Factory $factory */

use App\OrdersProducts;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(OrdersProducts::class, function (Faker $faker) {
    return [
//        'orders_id' => $orders_id,
//        'products_id' => $products_id,
        'price' => mt_rand(1, 25),
        'quantity' => mt_rand(2, 10)
    ];
});
