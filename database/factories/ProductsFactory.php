<?php

/** @var Factory $factory */

use App\Products;
use Bezhanov\Faker\Provider\Commerce;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Products::class, function (Faker $faker) {
    $faker->addProvider(new Commerce($faker));

    return [
        'name' => $faker->productName,
        'description' => $faker->text,
        'price' => mt_rand(1, 25),
        'status' => 'active'
    ];
});
