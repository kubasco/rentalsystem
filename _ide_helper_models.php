<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\OrdersProducts
 *
 * @property int $id
 * @property int $orders_id
 * @property int $products_id
 * @property float $price
 * @property int $quantity
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts whereOrdersId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts whereProductsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrdersProducts whereUpdatedAt($value)
 */
	class OrdersProducts extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $phone
 * @property string|null $address
 * @property string|null $zip_code
 * @property string|null $city
 * @property string $type
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereZipCode($value)
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * App\Users
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $phone
 * @property string|null $address
 * @property string|null $zip_code
 * @property string|null $city
 * @property string $type
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Orders[] $orders
 * @property-read int|null $orders_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Users newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Users newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Users query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Users whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Users whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Users whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Users whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Users whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Users whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Users whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Users wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Users wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Users whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Users whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Users whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Users whereZipCode($value)
 */
	class Users extends \Eloquent {}
}

namespace App{
/**
 * App\Products
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property float|null $price
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Products newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Products newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Products query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Products whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Products whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Products whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Products whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Products whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Products wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Products whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Products whereUpdatedAt($value)
 */
	class Products extends \Eloquent {}
}

namespace App{
/**
 * App\Orders
 *
 * @property int $id
 * @property int $users_id
 * @property string|null $book_from
 * @property string|null $book_to
 * @property string|null $start_date
 * @property string|null $end_date
 * @property float|null $paid
 * @property string $name
 * @property string $phone
 * @property string|null $email
 * @property string|null $comment
 * @property string $other_location
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\OrdersProducts[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Orders newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Orders newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Orders query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Orders whereBookFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Orders whereBookTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Orders whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Orders whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Orders whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Orders whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Orders whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Orders whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Orders whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Orders whereOtherLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Orders wherePaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Orders wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Orders whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Orders whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Orders whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Orders whereUsersId($value)
 */
	class Orders extends \Eloquent {}
}

