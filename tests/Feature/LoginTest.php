<?php

namespace Tests\Feature\Admin;

use App\Users;
use Tests\TestCase;

class LoginTest extends TestCase
{
    public function testViewLoginPage(): void
    {
        $response = $this->get(route('admin_login'));
        $response->assertSuccessful();
        $response->assertViewIs('auth.login');
    }

    public function testUserLogin(): void
    {
        $user = factory(Users::class)->make();
        $response = $this->actingAs($user)->get(route('admin_login'));
        $response->assertRedirect(route('admin_panel'));
    }

    public function testUserIsOkButPasswordIncorrect(): void
    {
        $user = factory(Users::class)->create([
            'password' => bcrypt('password'),
        ]);

        $response = $this->from(route('admin_login'))->post(route('admin_login'), [
            'email' => $user->email,
            'password' => 'wrongpassword',
        ]);

        $response->assertRedirect(route('admin_login'));
        $response->assertSessionHasErrors('email');
        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }
}
