<?php

namespace Tests\Feature\Admin;

use App\Products;
use App\Users;
use Tests\TestCase;

class UsersSingleTest extends TestCase
{
    public function testUserAdminLoggedInEditUser()
    {
        $userType = 'admin';
        $newName = 'Edited UserName';
        $userAdmin = factory(Users::class)->create([
            'type' => $userType
        ]);
        $userToChange = factory(Users::class)->create();
        $userToChange->name = $newName;

        $this
            ->actingAs($userAdmin)
            ->put(route('admin_update_user', $userToChange->id), $userToChange->toArray());
        $this->assertDatabaseHas('users',['id'=> $userToChange->id , 'name' => $newName]);
    }

    public function testUserAdminLoggedInAddNewUserWorker()
    {
        $userType = 'admin';
        $newName = 'New UserName';
        $userAdmin = factory(Users::class)->create([
            'type' => $userType
        ]);
        $newUser = factory(Users::class)->make([
            'name' => $newName,
        ]);
        $newUser = $newUser->toArray();
        $newUser['password'] = '12345678';

        $this
            ->actingAs($userAdmin)
            ->post(route('admin_save_new_user'), $newUser);
        $this->assertDatabaseHas('users',['name' => $newName]);
    }
}
