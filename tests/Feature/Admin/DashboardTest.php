<?php

namespace Tests\Feature\Admin;

use App\Users;
use Tests\TestCase;

class DashboardTest extends TestCase
{
    public function testUserLoggedInVisitDashboard(): void
    {
        $user = factory(Users::class)->create();

        $response = $this
            ->actingAs($user)
            ->withSession(['foo' => 'bar'])
            ->get(route('admin_panel'));
        $response->assertSuccessful();
        $response->assertViewIs('auth.dashboard');
    }
}
