<?php

namespace Tests\Feature\Admin;

use App\Users;
use Tests\TestCase;

class UsersTest extends TestCase
{
    public function testUserLoggedInVisitUsers(): void
    {
        $user = factory(Users::class)->create();

        $response = $this
            ->actingAs($user)
            ->withSession(['foo' => 'bar'])
            ->get(route('admin_users'));
        $response->assertSuccessful();
        $response->assertViewIs('auth.users');
    }
}
