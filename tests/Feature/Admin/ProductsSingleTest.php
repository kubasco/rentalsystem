<?php

namespace Tests\Feature\Admin;

use App\Products;
use App\Users;
use Tests\TestCase;

class ProductsSingleTest extends TestCase
{
    public function testUserLoggedInVisitProductsSingle()
    {
        $user = factory(Users::class)->create();
        $product = factory(Products::class)->create();

        $response = $this
            ->actingAs($user)
            ->withSession(['foo' => 'bar'])
            ->get(route('admin_show_product', $product->id));
        $response->assertSuccessful();
        $response->assertViewIs('auth.product_page');
    }

    public function testUserLoggedInEditProduct()
    {
        $newName = 'Updated Name of Product';
        $user = factory(Users::class)->create();
        $product = factory(Products::class)->create();
        $product->name = $newName;

        $this
            ->actingAs($user)
            ->put(route('admin_update_product', $product->id), $product->toArray());
        $this->assertDatabaseHas('products',['id'=> $product->id , 'name' => $newName]);
    }

    public function testUserLoggedInAddNewProduct()
    {
        $newName = 'Name of New Product';
        $user = factory(Users::class)->create();
        $product = factory(Products::class)->make();
        $product->name = $newName;

        $this
            ->actingAs($user)
            ->post(route('admin_save_new_product'), $product->toArray());
        $this->assertDatabaseHas('products',['name' => $newName]);
    }
}
