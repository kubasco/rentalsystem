<?php

namespace Tests\Feature\Admin;

use App\Orders;
use App\Products;
use App\Users;
use Tests\TestCase;

class OrdersSingleTest extends TestCase
{
    public function testUserLoggedInVisitOrdersSingle()
    {
        $user = factory(Users::class)->create();
        $order = factory(Orders::class)->create([
            'users_id' => $user->id
        ]);

        $response = $this
            ->actingAs($user)
            ->withSession(['foo' => 'bar'])
            ->get(route('admin_show_order', $order->id));
        $response->assertSuccessful();
        $response->assertViewIs('auth.order_page');
    }

    public function testUserLoggedInEditOrderPersonalData()
    {
        $newName = 'Updated Name of Order';
        $user = factory(Users::class)->create();
        $order = factory(Orders::class)->create([
            'users_id' => $user->id
        ]);
        $order->name = $newName;
        $order->phone = '500500500';
        $order->document_number = '1234567890';
        $order->document_type = 'identity_card';
        $order->book_from_date = '2020-01-01';
        $order->book_from_hour = 1;
        $order->book_from_minute = 2;
        $order->book_to_date = '2020-01-02';
        $order->book_to_hour = 3;
        $order->book_to_minute = 8;
        $order->other_location = 'yes';
        $order->return_location = 'yes';

        $this
            ->actingAs($user)
            ->put(route('admin_update_order', $order->id), $order->toArray());
        $this->assertDatabaseHas('orders',['id'=> $order->id, 'name' => $newName]);
    }

    public function testUserLoggedInEditOrderPersonalDataOtherLocationPickupAt()
    {
        $newName = 'Updated Name of Order 2';
        $testAddress = 'Street 12, City';
        $user = factory(Users::class)->create();
        $order = factory(Orders::class)->create([
            'users_id' => $user->id
        ]);
        $order->name = $newName;
        $order->phone = '500500500';
        $order->document_number = '1234567890';
        $order->document_type = 'identity_card';
        $order->book_from_date = '2020-01-01';
        $order->book_from_hour = 1;
        $order->book_from_minute = 2;
        $order->book_to_date = '2020-01-02';
        $order->book_to_hour = 3;
        $order->book_to_minute = 8;
        $order->other_location = 'no';
        $order->other_location_data = $testAddress;
        $order->return_location = 'no';
        $order->return_location_data = $testAddress;

        $this
            ->actingAs($user)
            ->put(route('admin_update_order', $order->id), $order->toArray());
        $this->assertDatabaseHas('orders',[
            'id'=> $order->id,
            'name' => $newName,
            'other_location' => $testAddress,
            'return_location' => $testAddress
        ]);
    }

    public function testUserLoggedInEditOrderProducts()
    {
        $newName = 'Test Product Name';
        $user = factory(Users::class)->create();
        $order = factory(Orders::class)->create(['users_id' => $user->id]);
        $product = factory(Products::class)->create();
        $product->name = $newName;
        $product->quantity = 2;

        $this
            ->actingAs($user)
            ->put(route('admin_update_order_products', $product->id), ['products' => json_encode([$product->toArray()])]);
        $this->assertDatabaseHas('orders_products',['id'=> $product->id , 'products_id' => $product->id, 'orders_id' => $order->id]);
    }

    public function testUserLoggedInAddNewOrder()
    {
        $newName = 'Name of New Product';
        $newPhone = '+48500500500';
        $user = factory(Users::class)->create();
        $order = factory(Orders::class)->create([
            'users_id' => $user->id,
            'name' => $newName,
            'phone' => $newPhone
        ]);
        $order->name = $newName;

        $this
            ->actingAs($user)
            ->post(route('admin_save_new_order'), $order->toArray());
        $this->assertDatabaseHas('orders',[
            'users_id' => $user->id,
            'name' => $newName,
            'phone' => $newPhone,
            ]);
    }
}
