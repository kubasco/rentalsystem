<?php

namespace Tests\Feature\Admin;

use App\Users;
use Tests\TestCase;

class ProductsTest extends TestCase
{
    public function testUserLoggedInVisitProducts(): void
    {
        $user = factory(Users::class)->create();

        $response = $this
            ->actingAs($user)
            ->withSession(['foo' => 'bar'])
            ->get(route('admin_orders'));
        $response->assertSuccessful();
        $response->assertViewIs('auth.orders');
    }
}
