<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;


/**
 * App\Orders
 *
 * @property int $id
 * @property int $users_id
 * @property string|null $book_from
 * @property string|null $book_to
 * @property string|null $start_date
 * @property string|null $end_date
 * @property float|null $paid
 * @property string $name
 * @property string|null $address
 * @property string|null $document_number
 * @property string|null $document_type
 * @property string $phone
 * @property string|null $email
 * @property string|null $comment
 * @property string|null $other_location
 * @property string|null $return_location
 * @property string $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read Collection|OrdersProducts[] $products
 * @property-read int|null $products_count
 * @method static Builder|Orders newModelQuery()
 * @method static Builder|Orders newQuery()
 * @method static Builder|Orders query()
 * @method static Builder|Orders whereAddress($value)
 * @method static Builder|Orders whereBookFrom($value)
 * @method static Builder|Orders whereBookTo($value)
 * @method static Builder|Orders whereComment($value)
 * @method static Builder|Orders whereCreatedAt($value)
 * @method static Builder|Orders whereDeletedAt($value)
 * @method static Builder|Orders whereDocumentNumber($value)
 * @method static Builder|Orders whereDocumentType($value)
 * @method static Builder|Orders whereEmail($value)
 * @method static Builder|Orders whereEndDate($value)
 * @method static Builder|Orders whereId($value)
 * @method static Builder|Orders whereName($value)
 * @method static Builder|Orders whereOtherLocation($value)
 * @method static Builder|Orders wherePaid($value)
 * @method static Builder|Orders wherePhone($value)
 * @method static Builder|Orders whereReturnLocation($value)
 * @method static Builder|Orders whereStartDate($value)
 * @method static Builder|Orders whereStatus($value)
 * @method static Builder|Orders whereUpdatedAt($value)
 * @method static Builder|Orders whereUsersId($value)
 * @mixin Eloquent
 */
class Orders extends Model
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'users_id', 'book_from', 'book_to', 'start_date', 'end_date', 'paid', 'name',
        'address', 'document_number', 'document_type', 'phone', 'email', 'comment',
        'other_location', 'return_location', 'status'
    ];

    protected $dates = ['deleted_at'];

    protected $hidden = ['created_at', 'updated_at'];

    /**
     * Get the products of order.
     */
    public function products(): HasMany
    {
        return $this->hasMany(OrdersProducts::class);
    }
}
