<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Http\Controllers\Controller;
use App\Users;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Config;
use Illuminate\View\View;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return Factory|View
     */
    public function get(): View
    {
        $usersTypes = Config::get('users.types');

        $users[$usersTypes['admin']] = Users::where(['type' => $usersTypes['admin']])->get();
        $users[$usersTypes['worker']] = Users::where(['type' => $usersTypes['worker']])->get();
        $users[$usersTypes['partner']] = Users::where(['type' => $usersTypes['partner']])->get();

        return view('auth.users', [
            'users' => $users,
            'users_types' => $usersTypes
        ]);
    }
}
