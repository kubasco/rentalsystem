<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Http\Controllers\Controller;
use App\Orders;
use App\Products;
use App\Users;
use Illuminate\Contracts\Support\Renderable;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function get()
    {
        $users = Users::all()->count();
        $orders = Orders::all()->count();
        $products = Products::all()->count();

        return view('auth.dashboard', [
            'users' => $users,
            'orders' => $orders,
            'products' => $products
        ]);
    }
}
