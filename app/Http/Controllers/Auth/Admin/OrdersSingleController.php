<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Http\Controllers\Controller;
use App\Orders;
use App\OrdersProducts;
use App\Products;
use App\Users;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class OrdersSingleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Int $id
     * @return View
     */
    public function get(Int $id): view
    {
        $order = Orders::findOrFail($id);

        if ($order->users_id !== null) {
            $client = Users::find($order->users_id);
        } else {
            $client = null;
        }

        $productsInOrder = Products::whereIn('id', $order->products->pluck('products_id')->toArray())->get()->keyBy('id')->toArray();

        return view('auth.order_page', [
            'order' => $order,
            'client' => $client,
            'order_products' => $order->products,
            'products_data_by_id' => $productsInOrder
        ]);
    }

    /**
     * @param Int $id
     * @return View
     */
    public function editOrder(Int $id): view
    {
        $order = Orders::findOrFail($id);

        return view('auth.order_page_edit', [
            'order' => $order,
            'statuses' => self::statuses,
            'documentTypes' => self::documentTypes
        ]);
    }

    /**
     * @param Request $request
     * @param Int $id
     * @return RedirectResponse|Redirector
     * @throws ValidationException
     */
    public function updateOrder(Request $request, Int $id)
    {
        $request->merge(['book_from' => $this->prepareDateTime($request, 'book_from')]);
        $request->merge(['book_to' => $this->prepareDateTime($request, 'book_to')]);

        $rules = [
            'book_from' => 'required|date_format:Y-m-d H:i:s',
            'book_to' => 'required|date_format:Y-m-d H:i:s|after:book_from',
//            'start_date' => 'date_format:Y-m-d H:i:s',
//            'end_date' => 'date_format:Y-m-d H:i:s',
            'paid' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
            'name' => 'required|string|max:128',
            'address' => 'nullable|string|max:512',
            'document_number' => 'required|string|max:128',
            'document_type' => 'required|in:identity_card,driving_license',
            'phone' => 'required|string|max:32',
            'email' => 'nullable|string|max:128',
            'comment' => 'string|max:5000',
            'other_location' => 'nullable|max:8',
            'other_location_data' => 'max:5000|required_if:other_location,==,no',
            'return_location' => 'nullable|max:8',
            'return_location_data' => 'max:5000|required_if:return_location,==,no',
            'status' => 'required|in:active,disabled'
        ];

        $names = [
            'book_from' => __('dashboard.book_from'),
            'book_to' => __('dashboard.book_to'),
            'paid' => __('dashboard.payed'),
            'name' => __('dashboard.who_order'),
            'address' => __('dashboard.client_address'),
            'document_number' => __('dashboard.document_number'),
            'document_type' => __('dashboard.document_type'),
            'dashboard.document_type' => __('dashboard.document_type'),
            'phone' => __('dashboard.phone'),
            'comment' => __('dashboard.notes'),
            'other_location' => __('dashboard.pickup_at_partner'),
            'other_location_data' => __('dashboard.pickup_at'),
            'return_location' => __('dashboard.return_location'),
            'return_location_data' => __('dashboard.return_location_at_partner'),
            'dashboard.driving_license' => __('dashboard.driving_license'),
            'dashboard.identity_card' => __('dashboard.identity_card')
        ];

        $this->validate($request, $rules, [], $names);

        if ($request->get('other_location') === 'yes') {
            $request->merge([
                'other_location_data' => null,
                'other_location' => null
            ]);
        } else {
            $request->merge([
                'other_location' => $request->get('other_location_data'),
            ]);
        }

        if ($request->get('return_location') === 'yes') {
            $request->merge([
                'return_location_data' => null,
                'return_location' => null
            ]);
        } else {
            $request->merge([
                'return_location' => $request->get('return_location_data'),
            ]);
        }

        $order = Orders::findOrFail($id);

        $order->book_from = $request->get('book_from');
        $order->book_to = $request->get('book_to');
        $order->paid = $request->get('paid');
        $order->name = $request->get('name');
        $order->address = $request->get('address');
        $order->document_number = $request->get('document_number');
        $order->document_type = $request->get('document_type');
        $order->phone = $request->get('phone');
        $order->email = $request->get('email');
        $order->comment = $request->get('comment');
        $order->other_location = $request->get('other_location');
        $order->return_location = $request->get('return_location');
        $order->status = $request->get('status');

        $order->save();

        return redirect(route('admin_show_order', $id))->with('success', __('dashboard.success_operation'))->withInput();
    }

    /**
     * @return View
     */
    public function addNewOrder(): view
    {
        return view('auth.order_page_new', [
            'statuses' => self::statuses,
            'documentTypes' => self::documentTypes
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Redirector
     * @throws ValidationException
     */
    public function saveNewOrder(Request $request)
    {
        $request->merge(['book_from' => $this->prepareDateTime($request, 'book_from')]);
        $request->merge(['book_to' => $this->prepareDateTime($request, 'book_to')]);

        $rules = [
            'book_from' => 'required|date_format:Y-m-d H:i:s',
            'book_to' => 'required|date_format:Y-m-d H:i:s|after:book_from',
//            'start_date' => 'date_format:Y-m-d H:i:s',
//            'end_date' => 'date_format:Y-m-d H:i:s',
            'paid' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
            'name' => 'required|string|max:128',
            'address' => 'nullable|string|max:512',
            'document_number' => 'required|string|max:128',
            'document_type' => 'required|in:identity_card,driving_license',
            'phone' => 'required|string|max:32',
            'email' => 'nullable|string|max:128',
            'comment' => 'string|max:5000',
            'other_location' => 'nullable|max:8',
            'other_location_data' => 'max:5000|required_if:other_location,==,no',
            'return_location' => 'nullable|max:8',
            'return_location_data' => 'max:5000|required_if:return_location,==,no',
            'status' => 'required|in:active,disabled'
        ];

        $names = [
            'book_from' => __('dashboard.book_from'),
            'book_to' => __('dashboard.book_to'),
            'paid' => __('dashboard.payed'),
            'name' => __('dashboard.who_order'),
            'address' => __('dashboard.client_address'),
            'document_number' => __('dashboard.document_number'),
            'document_type' => __('dashboard.document_type'),
            'dashboard.document_type' => __('dashboard.document_type'),
            'phone' => __('dashboard.phone'),
            'comment' => __('dashboard.notes'),
            'other_location' => __('dashboard.pickup_at_partner'),
            'other_location_data' => __('dashboard.pickup_at'),
            'return_location' => __('dashboard.return_location'),
            'return_location_data' => __('dashboard.return_location_at_partner'),
            'dashboard.driving_license' => __('dashboard.driving_license'),
            'dashboard.identity_card' => __('dashboard.identity_card')
        ];

        $this->validate($request, $rules, [], $names);

        if ($request->get('other_location') === 'yes') {
            $request->merge([
                'other_location_data' => null,
                'other_location' => null
            ]);
        } else {
            $request->merge([
                'other_location' => $request->get('other_location_data'),
            ]);
        }

        if ($request->get('return_location') === 'yes') {
            $request->merge([
                'return_location_data' => null,
                'return_location' => null
            ]);
        } else {
            $request->merge([
                'return_location' => $request->get('return_location_data'),
            ]);
        }

        $order = new Orders;

        $order->users_id = auth()->user()->id;

        $order->book_from = $request->get('book_from');
        $order->book_to = $request->get('book_to');
        $order->paid = $request->get('paid');
        $order->name = $request->get('name');
        $order->address = $request->get('address');
        $order->document_number = $request->get('document_number');
        $order->document_type = $request->get('document_type');
        $order->phone = $request->get('phone');
        $order->email = $request->get('email');
        $order->comment = $request->get('comment');
        $order->other_location = $request->get('other_location');
        $order->return_location = $request->get('return_location');
        $order->status = $request->get('status');

        $order->save();

        return redirect(route('admin_show_order', $order->id))->with('success', __('dashboard.success_operation'))->withInput();
    }

    /**
     * @param Int $id
     * @return View
     */
    public function editOrderProducts(Int $id): view
    {
        $order = Orders::findOrFail($id);
        $products = Products::where(['status' => 'active'])->get();
        $ordersProducts = DB::table('orders_products as c1')
            ->join('products as c2', 'c2.id', '=', 'c1.products_id')
            ->select('c1.products_id as id', 'c1.orders_id', 'c1.price', 'c1.quantity', 'c2.name')
            ->where('c1.orders_id', '=',$id)
            ->get();
        $productsInOrder = Products::whereIn('id', $order->products->pluck('products_id')->toArray())->get()->keyBy('id')->toArray();

        return view('auth.order_page_products_edit', [
            'order' => $order,
            'products' => $products,
            'orders_products' => $ordersProducts,
            'products_data_by_id' => $productsInOrder
        ]);
    }

    /**
     * @param Request $request
     * @param Int $id
     * @return RedirectResponse|Redirector
     */
    public function updateOrderProducts(Request $request, Int $id)
    {
        $this->OrdersProductsUpdateOrDelete($request, $id);

        return redirect(route('admin_show_order', $id))->with('success', __('dashboard.success_operation'))->withInput();
    }

    /**
     * @param $time
     * @return string
     */
    private function setProperTime($time): string
    {
        return (strlen($time) < 2) ? '0' . $time : $time;
    }

    /**
     * @param Request $request
     * @param $case
     * @return string
     */
    private function prepareDateTime(Request $request, $case): string
    {
        switch ($case) {
            case 'book_from':
                return $request->get('book_from_date') . ' '
                    . $this->setProperTime($request->get('book_from_hour'))
                    . ':'
                    . $this->setProperTime($request->get('book_from_minute'))
                    . ':00';
            case 'book_to':
                return $request->get('book_to_date') . ' '
                    . $this->setProperTime($request->get('book_to_hour'))
                    . ':'
                    . $this->setProperTime($request->get('book_to_minute'))
                    . ':00';
        }
    }

    /**
     * @param Request $request
     * @param Int $id
     */
    private static function OrdersProductsUpdateOrDelete(Request $request, Int $id)
    {
        $productFromRequest = json_decode($request->get('products'));
        $ordersProducts = OrdersProducts::where(['orders_id' => $id])->get();
        $ordersProductsArray = $ordersProducts->keyBy('products_id')->toArray();

        if(!is_null($ordersProductsArray)){
            // DELETE OLD ITEMS
            foreach($ordersProducts as $data){
                $notFound = true;
                foreach($productFromRequest as $dataIn) {
                    if ($data->products_id == $dataIn->id) {
                        $notFound = false;
                    }
                }
                if ($notFound){
                    $data->delete();
                }
            }

            //UPDATE ITEMS
            foreach($ordersProducts as $data){
                foreach($productFromRequest as $formProduct) {
                    if ($data->products_id == $formProduct->id) {
                        $data->price = $formProduct->price;
                        $data->quantity = $formProduct->quantity;
                        $data->save();
                    }
                }
            }
        }

        // ADD NEW ITEMS
        foreach($productFromRequest as $data){
            if(!array_key_exists($data->id, $ordersProductsArray) || is_null($ordersProductsArray)){
                $productExists = Products::find($data->id);
                if($productExists){
                    OrdersProducts::create([
                        'orders_id' => $id,
                        'products_id' => $data->id,
                        'price' => $data->price,
                        'quantity' => $data->quantity,
                    ]);
                }
            }
        }
    }
}
