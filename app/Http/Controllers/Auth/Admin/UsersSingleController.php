<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Http\Controllers\Controller;
use App\Users;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class UsersSingleController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Int $id
     * @return View
     */
    public function editUser(Int $id): view
    {
        $user = User::findOrFail($id);
        $userTypes = Config::get('users.types');
        $userTypes = $this->allTypesOnlyIfAdmin($userTypes);

        return view('auth.user_page_edit', [
            'user' => $user,
            'user_types' => $userTypes
        ]);
    }

    /**
     * @param Request $request
     * @param Int $id
     * @return RedirectResponse|Redirector
     * @throws ValidationException
     */
    public function updateUser(Request $request, Int $id)
    {
        $userTypes = implode(',', Config::get('users.types'));

        $rules = [
            'name' => 'required|string|max:128',
            'email' => 'required|string|max:128|unique:users,email,'.$id,
            'password' => 'min:8|string|max:256|nullable',
            'phone' => 'min:7|string|max:32|nullable',
            'address' => 'string|max:128|nullable',
            'zip_code' => 'string|max:16|nullable',
            'city' => 'string|max:128|nullable',
            'type' => 'required|in:' . $userTypes
        ];

        $names = [
            'name' => __('dashboard.name'),
            'email' => __('dashboard.email'),
            'password' => __('dashboard.password'),
            'phone' => __('dashboard.phone'),
            'address' => __('dashboard.address'),
            'zip_code' => __('dashboard.zip_code'),
            'city' => __('dashboard.city')
        ];

        $this->validate($request, $rules, [], $names);


        $user = User::findOrFail($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        if($request->get('password') !== null || $user->password === $request->get('password')){
            $user->password = Hash::make($request->get('password'));
        }
        $user->phone = $request->get('phone');
        $user->address = $request->get('address');
        $user->zip_code = $request->get('zip_code');
        $user->city = $request->get('city');
        $user->type = $request->get('type');

        $user->save();

        return redirect(route('admin_users'))->with('success', __('dashboard.success_operation'))->withInput();
    }

    /**
     * @return View
     */
    public function addNewUser(): view
    {
        $userTypes = Config::get('users.types');
        $userTypes = $this->allTypesOnlyIfAdmin($userTypes);

        return view('auth.user_page_new', [
            'user_types' => $userTypes
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Redirector
     * @throws ValidationException
     */
    public function saveNewUser(Request $request)
    {
        $userTypes = implode(',', Config::get('users.types'));

        $rules = [
            'name' => 'required|string|max:128',
            'email' => 'required|string|max:128|unique:users,email',
            'password' => 'required|min:8|string|max:256',
            'phone' => 'min:7|string|max:32|nullable',
            'address' => 'string|max:128|nullable',
            'zip_code' => 'string|max:16|nullable',
            'city' => 'string|max:128|nullable',
            'type' => 'required|in:' . $userTypes
        ];

        $names = [
            'name' => __('dashboard.name'),
            'email' => __('dashboard.email'),
            'password' => __('dashboard.password'),
            'phone' => __('dashboard.phone'),
            'address' => __('dashboard.address'),
            'zip_code' => __('dashboard.zip_code'),
            'city' => __('dashboard.city')
        ];

        $this->validate($request, $rules, [], $names);

        $user = new User();

        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
        $user->phone = $request->get('phone');
        $user->address = $request->get('address');
        $user->zip_code = $request->get('zip_code');
        $user->city = $request->get('city');
        $user->type = $request->get('type');

        $user->save();

        return redirect(route('admin_users'))->with('success', __('dashboard.success_operation'))->withInput();
    }

    /**
     * @param Int $id
     * @return RedirectResponse|Redirector
     */
    public function deleteUser(Int $id)
    {
        Users::findOrFail($id)->delete();
        return redirect(route('admin_users'))->with('success', __('dashboard.success_operation'))->withInput();
    }

    /**
     * @param array $userTypes
     * @return Array
     */
    private function allTypesOnlyIfAdmin(Array $userTypes): Array
    {
        if (auth()->user()->type !== 'admin') {
            unset($userTypes['admin']);
        }
        return $userTypes;
    }
}
