<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Http\Controllers\Controller;
use App\Products;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class ProductsSingleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Int $id
     * @return View
     */
    public function get(Int $id): view
    {
        $product = Products::findOrFail($id);

        return view('auth.product_page', [
            'product' => $product
        ]);
    }

    /**
     * @param Int $id
     * @return View
     */
    public function editProduct(Int $id): view
    {
        $product = Products::findOrFail($id);

        return view('auth.product_page_edit', [
            'product' => $product,
            'statuses' => self::statuses
        ]);
    }

    /**
     * @param Request $request
     * @param Int $id
     * @return RedirectResponse|Redirector
     * @throws ValidationException
     */
    public function updateProduct(Request $request, Int $id)
    {
        $rules = [
            'name' => 'required|string',
            'description' => 'string',
            'price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'status' => 'required|in:active,disabled'
        ];

        $names = [
            'name' => __('dashboard.product_name'),
            'description' => __('dashboard.description'),
            'price' => __('dashboard.price'),
        ];

        $this->validate($request, $rules, [], $names);

        $product = Products::findOrFail($id);

        $product->name = $request->get('name');
        $product->description = $request->get('description');
        $product->price = $request->get('price');
        $product->status = $request->get('status');

        $product->save();

        return redirect(route('admin_show_product', $id))->with('success', __('dashboard.success_operation'))->withInput();
    }


    public function newProduct(): view
    {
        return view('auth.product_page_new', [
            'statuses' => self::statuses
        ]);
    }


    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'description' => 'string',
            'price' => 'required|integer',
            'status' => 'required|in:active,disabled'
        ]);

        $product = new Products();

        $product->name = $request->get('name');
        $product->description = $request->get('description');
        $product->price = $request->get('price');
        $product->status = $request->get('status');

        $product->save();

        return redirect(route('admin_products'))->with('success', __('dashboard.success_operation'))->withInput();
    }
}
