<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Http\Controllers\Controller;
use App\Products;
use Illuminate\View\View;

class ProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return View
     */
    public function get(): View
    {
        $products = Products::orderBy('id', 'DESC')->paginate(self::resultsPerPage);

        return view('auth.products', [
            'products' => $products
        ]);
    }
}
