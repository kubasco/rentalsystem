<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Http\Controllers\Controller;
use App\Orders;
use Illuminate\View\View;

class OrdersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return View
     */
    public function get(): View
    {
        $orders = Orders::paginate(self::resultsPerPage);

        return view('auth.orders', [
            'orders' => $orders
        ]);
    }
}
