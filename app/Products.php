<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;

/**
 * App\Products
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property float|null $price
 * @property string $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static Builder|Products newModelQuery()
 * @method static Builder|Products newQuery()
 * @method static Builder|Products query()
 * @method static Builder|Products whereCreatedAt($value)
 * @method static Builder|Products whereDeletedAt($value)
 * @method static Builder|Products whereDescription($value)
 * @method static Builder|Products whereId($value)
 * @method static Builder|Products whereName($value)
 * @method static Builder|Products wherePrice($value)
 * @method static Builder|Products whereStatus($value)
 * @method static Builder|Products whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Products extends Model
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'price', 'status'
    ];

    protected $dates = ['deleted_at'];

    protected $hidden = ['created_at', 'updated_at'];
}
