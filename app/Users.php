<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;

/**
 * App\Users
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $phone
 * @property string|null $address
 * @property string|null $zip_code
 * @property string|null $city
 * @property string|null $type
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read Collection|Orders[] $orders
 * @property-read int|null $orders_count
 * @method static Builder|Users newModelQuery()
 * @method static Builder|Users newQuery()
 * @method static Builder|Users query()
 * @method static Builder|Users whereAddress($value)
 * @method static Builder|Users whereCity($value)
 * @method static Builder|Users whereCreatedAt($value)
 * @method static Builder|Users whereDeletedAt($value)
 * @method static Builder|Users whereEmail($value)
 * @method static Builder|Users whereId($value)
 * @method static Builder|Users whereName($value)
 * @method static Builder|Users wherePassword($value)
 * @method static Builder|Users wherePhone($value)
 * @method static Builder|Users whereRememberToken($value)
 * @method static Builder|Users whereType($value)
 * @method static Builder|Users whereUpdatedAt($value)
 * @method static Builder|Users whereZipCode($value)
 * @mixin Eloquent
 */
class Users extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'address', 'zip_code', 'city', 'type'
    ];

    protected $dates = ['deleted_at'];

    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at'
    ];

    /**
     * Get the products of order.
     */
    public function orders(): HasMany
    {
        return $this->hasMany(Orders::class);
    }
}
