<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;

/**
 * App\OrdersProducts
 *
 * @property int $id
 * @property int $orders_id
 * @property int $products_id
 * @property float $price
 * @property int $quantity
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static Builder|OrdersProducts newModelQuery()
 * @method static Builder|OrdersProducts newQuery()
 * @method static Builder|OrdersProducts query()
 * @method static Builder|OrdersProducts whereCreatedAt($value)
 * @method static Builder|OrdersProducts whereDeletedAt($value)
 * @method static Builder|OrdersProducts whereId($value)
 * @method static Builder|OrdersProducts whereOrdersId($value)
 * @method static Builder|OrdersProducts wherePrice($value)
 * @method static Builder|OrdersProducts whereProductsId($value)
 * @method static Builder|OrdersProducts whereQuantity($value)
 * @method static Builder|OrdersProducts whereUpdatedAt($value)
 * @mixin Eloquent
 */
class OrdersProducts extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'orders_id', 'products_id', 'price', 'quantity'
    ];
}
