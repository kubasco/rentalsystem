<?php

Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{
    Route::get('/', 'Auth\LoginController@loginPage')->name('login_page');

    Route::group(['prefix' => 'admin'], function () {
        // Authentication
        Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin_login');
        Route::post('login', 'Auth\LoginController@login');
        Route::post('logout', 'Auth\LoginController@logout')->name('admin_logout');

        // Admin Pages
        Route::get('/', 'Auth\Admin\DashboardController@get')->name('admin_panel');
        Route::get('users', 'Auth\Admin\UsersController@get')->name('admin_users');
        Route::get('users/edit/{id}', 'Auth\Admin\UsersSingleController@editUser')->name('admin_edit_user')->where('id', '[0-9]+');
        Route::put('users/edit/{id}', 'Auth\Admin\UsersSingleController@updateUser')->name('admin_update_user')->where('id', '[0-9]+');
        Route::get('users/add', 'Auth\Admin\UsersSingleController@addNewUser')->name('admin_add_new_user');
        Route::post('users/add', 'Auth\Admin\UsersSingleController@saveNewUser')->name('admin_save_new_user');
        Route::get('users/delete/{id}', 'Auth\Admin\UsersSingleController@deleteUser')->name('admin_delete_user')->where('id', '[0-9]+');
        Route::get('products', 'Auth\Admin\ProductsController@get')->name('admin_products');
        Route::get('products/{id}', 'Auth\Admin\ProductsSingleController@get')->name('admin_show_product')->where('id', '[0-9]+');
        Route::get('products/edit/{id}', 'Auth\Admin\ProductsSingleController@editProduct')->name('admin_edit_product')->where('id', '[0-9]+');
        Route::put('products/edit/{id}', 'Auth\Admin\ProductsSingleController@updateProduct')->name('admin_update_product')->where('id', '[0-9]+');
        Route::get('products/new', 'Auth\Admin\ProductsSingleController@newProduct')->name('admin_new_product');
        Route::post('products/new', 'Auth\Admin\ProductsSingleController@create')->name('admin_save_new_product');
        Route::get('orders', 'Auth\Admin\OrdersController@get')->name('admin_orders');
        Route::get('orders/{id}', 'Auth\Admin\OrdersSingleController@get')->name('admin_show_order')->where('id', '[0-9]+');
        Route::get('orders/edit/{id}', 'Auth\Admin\OrdersSingleController@editOrder')->name('admin_edit_order')->where('id', '[0-9]+');
        Route::put('orders/edit/{id}', 'Auth\Admin\OrdersSingleController@updateOrder')->name('admin_update_order')->where('id', '[0-9]+');
        Route::get('orders/edit/products/{id}', 'Auth\Admin\OrdersSingleController@editOrderProducts')->name('admin_edit_order_products')->where('id', '[0-9]+');
        Route::put('orders/edit/products/{id}', 'Auth\Admin\OrdersSingleController@updateOrderProducts')->name('admin_update_order_products')->where('id', '[0-9]+');
        Route::get('orders/add', 'Auth\Admin\OrdersSingleController@addNewOrder')->name('admin_add_new_order');
        Route::post('orders/add', 'Auth\Admin\OrdersSingleController@saveNewOrder')->name('admin_save_new_order');

        // Registration
    //    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    //    Route::post('register', 'Auth\RegisterController@register');

        // Password Reset
    //    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    //    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    //    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    //    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    });

});
